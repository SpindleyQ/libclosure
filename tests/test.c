#include "closure.h"

int foo(int a, int b)
{
  printf("%d%d\n", a, b);
}

int foo2(char a, short b, int c)
{
  printf("%c%d%d\n", a, b, c);
  return c;
}

static size_t foo_size[] = {sizeof(int), sizeof(int), 0};
static size_t foo2_size[] = {sizeof(char), sizeof(short), sizeof(int), 0};
int main(void)
{
  void (*bla1)();
  void (*bla2)(int, int);
  int  (*bla3)(int);

  int bla1_binding[] = {1 | MAP_BOUND, 0 | MAP_BOUND};
  int bla2_binding[] = {1, 0};
  int bla3_binding[] = {0 | MAP_BOUND, 1 | MAP_BOUND, 0};

  bla1 = create_closure(foo, foo_size, bla1_binding, 4, 7);
  bla2 = create_closure(foo, foo_size, bla2_binding);
  bla3 = create_closure(foo2, foo2_size, bla3_binding, 'c', 100);

  bla1();
  bla2(1,2);
  printf("%d\n", bla3(500000));

  return 0;
}
