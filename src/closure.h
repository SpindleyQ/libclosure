/*
    This file is part of libclosure.

    libclosure is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation; either version 2.1 of the 
    License, or (at your option) any later version.

    libclosure is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with libclosure; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __CLOSURE_H
#define __CLOSURE_H

#include <stdio.h>

/* Bitwise-OR an index with MAP_ARG to indicate the parameter of the 
 * new function that the current parameter of the old function 
 * corresponds to.
 */
#define MAP_ARG   0x0000

/* Bitwise-OR an index with MAP_BOUND to indicate the extra parameter 
 * passed to create_closure which the new function should substitute.
 */

#define MAP_BOUND 0x8000

void *create_closure(void *func, size_t *argsizes, unsigned int *mapping, ...);

#endif
