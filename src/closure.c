/*
    This file is part of libclosure.

    libclosure is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2.1 of the
    License, or (at your option) any later version.

    libclosure is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with libclosure; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "closure.h"
#include <lightning.h>
#include <stdlib.h>
#include <stdarg.h>

typedef union {
  unsigned char  c;
  unsigned short s;
  unsigned int   i;
  unsigned long  l;
  void *         p;
} arg_t;

void *create_closure(void *func, size_t *argsizes, unsigned int *mapping, ...)
{
  jit_insn *codeBuf = malloc(512 * sizeof(jit_insn));
  void *closure = jit_set_ip(codeBuf).iptr;
  int numFuncArgs, numClosureArgs, numVarArgs;
  int i = 0;
  int j = 0;
  int arg_i;
  int map;
  va_list arg_list;
  int *arg_indexes;
  arg_t *var_args;

  numVarArgs = numFuncArgs = numClosureArgs = 0;
  for (i = 0; argsizes[i] != 0; i ++)
  {
    numFuncArgs ++;
    if (!(mapping[i] & MAP_BOUND))
    {
      numClosureArgs ++;
    }
    else
    {
      numVarArgs ++;
    }
  }
  arg_indexes = alloca(numClosureArgs * sizeof(int));
  var_args = alloca(numVarArgs * sizeof(arg_t));

  va_start (arg_list, mapping);
  for (i = 0; i < numVarArgs; i ++)
  {
    for (j = 0; j < numFuncArgs; j ++)
    {
      if (mapping[j] == i | MAP_BOUND)
      {
        if (argsizes[i] == sizeof(char))
        {
          var_args[i].c = va_arg(arg_list, unsigned int);
        }
        else if (argsizes[i] == sizeof(short))
        {
          var_args[i].s = va_arg(arg_list, unsigned int);       
        }
        else if (argsizes[i] == sizeof (int))
        {
          var_args[i].i = va_arg(arg_list, unsigned int);
        }
        else if (argsizes[i] == sizeof (long))
        {
          var_args[i].l = va_arg(arg_list, unsigned long);
        }
        else if (argsizes[i] == sizeof (void *))
        {
          var_args[i].p = va_arg(arg_list, void *);
        }
        break;
      }
    }
  }
  va_end(arg_list);
  jit_prolog(numClosureArgs);
  for (i = 0; i < numClosureArgs; i ++)
  {
    for (j = 0; j < numFuncArgs; j ++)
    {
      if (mapping[j] == i | MAP_ARG)
      {
        if (argsizes[j] == sizeof(char))
        {
          arg_indexes[i] = jit_arg_uc();
        }
        else if (argsizes[j] == sizeof(short))
        {
          arg_indexes[i] = jit_arg_us();
        }
        else if (argsizes[j] == sizeof (int))
        {
          arg_indexes[i] = jit_arg_ui();
        }
        else if (argsizes[j] == sizeof (long))
        {
          arg_indexes[i] = jit_arg_ul();
        }
        else if (argsizes[j] == sizeof (void *))
        {
          arg_indexes[i] = jit_arg_p();
        }       
      }
    }
  }
  jit_prepare(numFuncArgs);
  for (i = numFuncArgs - 1; i >= 0; i --)
  {
    if (mapping[i] & MAP_BOUND)
    {
      map = mapping[i] & ~MAP_BOUND;
      if (argsizes[i] == sizeof(char))
      {
        jit_movi_ui(JIT_R0, var_args[map].c);
      }
      else if (argsizes[i] == sizeof(short))
      {
        jit_movi_ui(JIT_R0, var_args[map].s);
      }
      else if (argsizes[i] == sizeof (int))
      {
        jit_movi_ui(JIT_R0, var_args[map].i);
      }
      else if (argsizes[i] == sizeof (long))
      {
        jit_movi_ul(JIT_R0, var_args[map].l);
      }
      else if (argsizes[i] == sizeof (void *))
      {
        jit_movi_p(JIT_R0, var_args[map].p);
      }
    }
    else
    {
      map = mapping[i];
      if (argsizes[i] == sizeof(char))
      {
        jit_getarg_uc(JIT_R0, arg_indexes[map]);
      }
      else if (argsizes[i] == sizeof(short))
      {
        jit_getarg_us(JIT_R0, arg_indexes[map]);
      }
      else if (argsizes[i] == sizeof (int))
      {
        jit_getarg_ui(JIT_R0, arg_indexes[map]);
      }
      else if (argsizes[i] == sizeof (long))
      {
        jit_getarg_ul(JIT_R0, arg_indexes[map]);
      }
      else if (argsizes[i] == sizeof (void *))
      {
        jit_getarg_p(JIT_R0, arg_indexes[map]);
      }
    }
    if (argsizes[i] == sizeof(char))
    {
      jit_pusharg_uc(JIT_R0);
    }
    else if (argsizes[i] == sizeof(short))
    {
      jit_pusharg_us(JIT_R0);
    }
    else if (argsizes[i] == sizeof (int))
    {
      jit_pusharg_ui(JIT_R0);
    }
    else if (argsizes[i] == sizeof (long))
    {
      jit_pusharg_ul(JIT_R0);
    }
    else if (argsizes[i] == sizeof (void *))
    {
      jit_pusharg_p(JIT_R0);
    }
  }
  jit_finish(func);
  jit_ret();
  jit_flush_code(codeBuf, jit_get_ip().ptr);

  return closure;
}

